# Table/Data Import
0. Create a database in your MySQL instance (I did this through Sequel Pro)
1. Set up your database connection in `config/database.php`
2. Run all migrations

# Generating Styles
The styles for the application are written in LESS, and use the NPM version of Bootstrap Less to pull in the base
bootstrap styles. To generate the styles perform the following actions:

0. Ensure that `Node.JS` and `npm` are installed and functioning correctly
1. Run `npm install`
2. Run `gulp` to build all necessary .css files

# Some Notes
If I were more familiar with Yii (or if I was doing this in Laravel), I would have probably done this completely
different. For a real world application like this, I would have completely decoupleded the front-end and back-end. The
back-end (the Yii/Laravel portion) would simply be a REST API that returned data or responses depending on the
situation. From there, the front-end would prompt users depending on the data/status received from the API. 

However, I try to limit the amount of time I spend on code challenges to around 2 hours, and I was not confident in 
being able to learn Yii as well as provide a complete application in that time limit. As it stands, I have spent about
4.5 hours total on the application. However, about 1.5 of that was spent just trying to get email notifications to play
nicely with MAMP/PHPMailer/Gmail.

Also, if this were a real world application, I would spend MUCH more time on the UI/UX portion, as I feel like this is
a perfect place to kind of go crazy with the UI with all kinds of nice effects and styles.

One last thing, I realize I have included the entire Yii framework plus my application. I felt it would be nice to
provide a complete package that could be placed in a folder and an appropriate vhost configuration be made, and that
would be it. I also realize I probably didn't set (or remove) all (un)necessary Yii configs.