<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div id="page-register" class="form">

	<div class="page-header">
		<h1>Register for the Loyalty Program</h1>
		<p>Please fill out the form below to register for our loyalty rewards program. You will receive a confirmation email after you have successfully signed up.</p>
	</div>

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'user-register-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// See class documentation of CActiveForm for details on this,
		// you need to use the performAjaxValidation()-method described there.
		'enableAjaxValidation'=>false,
		'htmlOptions' => [
			'class' => 'form-horizontal'
		]
	)); ?>
		<?php echo $form->errorSummary($model); ?>

		<div class="form-group">
			<?php echo $form->labelEx($model,'phone_number', ['class' => 'control-label col-sm-3'] ); ?>
			<div class="col-sm-9">
				<?php echo $form->textField($model,'phone_number', ['value' => Yii::app()->user->phone_number, 'class' => 'form-control']); ?>
				<?php echo $form->error($model,'phone_number', ['class' => 'help-block']); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'email', ['class' => 'control-label col-sm-3'] ); ?>
			<div class="col-sm-9">
				<?php echo $form->textField($model,'email', ['class' => 'form-control']); ?>
				<?php echo $form->error($model,'email', ['class' => 'help-block']); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'first_name', ['class' => 'control-label col-sm-3'] ); ?>
			<div class="col-sm-9">
				<?php echo $form->textField($model,'first_name', ['class' => 'form-control']); ?>
				<?php echo $form->error($model,'first_name', ['class' => 'help-block']); ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'last_name', ['class' => 'control-label col-sm-3']); ?>
			<div class="col-sm-9">
				<?php echo $form->textField($model,'last_name', ['class' => 'form-control']); ?>
				<?php echo $form->error($model,'last_name', ['class' => 'help-block']); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
				<span class="help-block">All fields are required.</span>
				<?php echo CHtml::submitButton('Create Account', ['class' => 'btn btn-default']); ?>
			</div>
		</div>
	<?php $this->endWidget(); ?>

</div>