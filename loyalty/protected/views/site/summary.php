<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div id="page-summary" class="form">

    <div class="page-header">
        <h1>Loyalty Summary</h1>
        <p>Thank you for checking in!</p>
    </div>

    <?php if(Yii::app()->user->hasFlash('check_in_must_wait')): ?>
        <div class="alert alert-danger">
            <?php echo Yii::app()->user->getFlash('check_in_must_wait'); ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-6">
            <section class="well">
                <header>Total Check Ins</header>
                <p class="total-check-ins text-center">
                    <?php echo $total_check_ins; ?>
                </p>
            </section>
        </div>
        <div class="col-sm-6">
            <section class="well">
                <header>Loyalty Points</header>
                <p class="total-loyalty-points text-center">
                    <?php echo $total_loyalty_points; ?>
                </p>
            </section>
        </div>
    </div>

    <a href="/" class="btn btn-danger btn-block">Logout</a>
</div>