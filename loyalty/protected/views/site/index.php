<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div class="page-header">
    <h1>Welcome to the Baker Loyalty Program!</h1>

    <p>Please enter your phone number to get started, or to collect loyalty points.</p>
</div>

<?php if(Yii::app()->user->hasFlash('no_phone_number_provided')): ?>
    <div class="alert alert-danger">
        <?php echo Yii::app()->user->getFlash('no_phone_number_provided'); ?>
    </div>
<?php endif; ?>

<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<form action="/site/checkin" method="post" class="form-horizontal">
			<div class="form-group">
				<label for="phone_number" class="col-sm-4 control-label">Phone Number</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="phone_number" id="phone_number">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-8">
					<button type="submit" class="btn btn-default">Check In</button>
				</div>
			</div>
		</form>
	</div>
</div>