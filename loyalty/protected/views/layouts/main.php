<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="language" content="en">

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div class="container">
	<?php echo $content; ?>
</div><!-- page -->

<footer>
    <div class="container">
        <hr>
        <div class="row" id="footer">
            <div class="col-xs-7">
                Copyright &copy; <?php echo date('Y'); ?> by Baker.
            </div>
            <div class="col-xs-5 text-right">
                All Rights Reserved.
            </div>
        </div><!-- footer -->
    </div>
</footer>

</body>
</html>
