<?php

class m161226_153827_create_check_ins_table extends CDbMigration
{
	public function up()
	{
	    $this->createTable('tbl_check_ins', [
	        'id' => 'pk',
            'user_id' => 'integer NOT NULL',
            'point_value' => 'integer NOT NULL',
            'date_created' => 'datetime'
        ]);
	}

	public function down()
	{
		echo "m161226_153827_create_check_ins_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}