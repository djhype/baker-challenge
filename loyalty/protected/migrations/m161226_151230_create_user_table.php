<?php

class m161226_151230_create_user_table extends CDbMigration
{
	public function up()
	{
	    $this->createTable('tbl_users', [
	        'id' => 'pk',
            'email' => 'string NOT NULL UNIQUE',
            'first_name' => 'string NOT NULL',
            'last_name' => 'string NOT NULL',
            'phone_number' => 'VARCHAR(16) NOT NULL'
        ]);
	}

	public function down()
	{
		$this->dropTable('tbl_users');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}