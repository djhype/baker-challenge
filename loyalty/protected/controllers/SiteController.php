<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array();
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

    /**
     * This is the action to attempt and check users in.
     *
     * If a user is found
     *   and the user has not checked in during the past 5 minutes
     *     the check in is logged
     *     the user is redirected to the summary page
     *   otherwise, the user is redirected to the summary page
     * Otherwise, the user is redirected to the register page
     */
	public function actionCheckIn() {
	    if(isset($_POST['phone_number']) && strlen($_POST['phone_number']) >= 10) {
            Yii::app()->user->setState('phone_number', $_POST['phone_number']);

            $user = User::model()->find('phone_number=:phone_number', [ ':phone_number' => $this->stripPhoneNumber($_POST['phone_number']) ]);

            if($user) {
                if($this->userHasWaitedFiveMinutes($user->id)) {
                    $this->checkInUser($user->id);
                } else {
                    Yii::app()->user->setFlash('check_in_must_wait', 'Oops! You must wait at least 5 minutes between check-ins.');
                }

                Yii::app()->user->setState('user_id', $user->id);
                $this->redirect('/site/summary');
            } else {
                $this->redirect('/site/register');
            }
        }

        // no/invalid phone number provided, return user to index with message
        Yii::app()->user->setFlash('no_phone_number_provided', 'Oops, you didn\'t provide us with a valid phone number!');
        $this->redirect('/site/index');
    }

    /**
     * Displays the register form, allowing the user to register their phone number
     */
    public function actionRegister() {
        $model=new User('register');

        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
            if($model->validate())
            {
                // replace all (), -, and spaces with blank to make phone number search consistent
                $model->phone_number = $this->stripPhoneNumber($model->phone_number);
                $model->save();

                $this->checkInUser($model->id, true);

                Yii::app()->user->setState('user_id', $model->id);

                $this->redirect('/site/summary');
            }
        }
        $this->render('register',array('model'=>$model));
    }

    /**
     * Displays the summary page, allowing the user to see their total check-ins and the number of loyalty points
     * accrued
     */
    public function actionSummary() {
        $user_id = Yii::app()->user->getState('user_id');
        $user = User::model()->findByPk($user_id);

        if(!$user) {
            $this->redirect('/');
        }

        $check_ins = CheckIn::model()->findAll('user_id=:user_id', [':user_id' => $user_id]);
        $total_check_ins = count($check_ins);
        $total_loyalty_points = 0;

        foreach($check_ins as $check_in) {
            $total_loyalty_points += $check_in->point_value;
        }

        $this->sendCheckInEmail($user->email, $total_loyalty_points);

        $this->render('summary', compact('user', 'total_check_ins', 'total_loyalty_points'));
    }

    /**
     * Strips a phone number of any non-numeric characters, allowing for easy searching in the DB
     *
     * @param bool $phone_number
     * @return mixed
     * @throws Exception
     */
    private function stripPhoneNumber($phone_number = false) {
        if(!$phone_number) {
            throw new Exception('Error: Phone number must be present');
        }

        return preg_replace('/[^0-9]/', '', $phone_number);
    }

    /**
     * This actually records the user check in. Abstracted into its own method so it can be used in multiple places.
     *
     * @param $user_id
     * @param bool $isFirstCheckIn
     */
    private function checkInUser($user_id, $isFirstCheckIn = false) {
        $check_in = new CheckIn();
        $check_in->user_id = $user_id;
        $check_in->point_value = $isFirstCheckIn ? CheckIn::FIRST_CHECK_IN_POINT_VALUE : CheckIn::CHECK_IN_POINT_VALUE;
        $check_in->date_created = date('Y-m-d H:i:s');
        $check_in->save();
    }

    /**
     * Checks to see if the user has waited for at least 5 minutes between check ins.
     *
     * @param $user_id
     * @return bool
     */
    private function userHasWaitedFiveMinutes($user_id)
    {
        $latest_check_in = CheckIn::model()->newest()->find('user_id=:user_id', [':user_id' => $user_id]);

        if($latest_check_in) {
            return strtotime($latest_check_in->date_created) < (time() - (5 * 60));
        }

        return true;
    }

    /**
     * Sends an email notification to the user letting them know they have checked in, and what their new loyalty point
     * balance is.
     *
     * @param $user_email_address
     * @param $total_loyalty_points
     */
    private function sendCheckInEmail($user_email_address, $total_loyalty_points)
    {
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;

        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = Yii::app()->params['mail']['username'];
        $mail->Password = Yii::app()->params['mail']['password'];
        $mail->SMTPSecure = 'ssl';
        $mail->Port = '465';
        $mail->SMTPDebug = 1;

        $mail->From = Yii::app()->params['adminEmail'];
        $mail->FromName = 'Baker';
        $mail->addAddress($user_email_address);

        $mail->Subject = "Thank you for checking in!";
        $mail->Body = "Thanks for checking in. You now have {$total_loyalty_points} loyalty points.";

        if(!$mail->send()) {
            // @todo in the real wold, we would log this
            echo "Message could not be sent.";
            echo "Mailer error: " . $mail->ErrorInfo;
        }
    }
}