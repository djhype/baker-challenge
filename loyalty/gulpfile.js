var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');

var paths = {
    'less': './assets/less/**/*.less'
}

gulp.task('less', function () {
    return gulp.src(paths.less)
        .pipe(less({
            paths: [
                './node_modules/bootstrap-less'
            ]
        }))
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('./css'));
});

gulp.task('watch', function () {
    gulp.watch(paths.less, ['less']);
})

gulp.task('default', ['less']);